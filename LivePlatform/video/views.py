from django.shortcuts import render
from datetime import datetime

def course(request, course_no=0):
    course_list=[
        {'name':'慈儀佛堂線上課程', 'tvcode0':'PUJP1oOIoXM'},
        {'name':'上週課程', 'tvcode1':'sXlWzMBrg64', 'tvcode2':'9YIv3R_jE8Q'},
    ]

    now = datetime.now()
    hour = now.timetuple().tm_hour
    course_no = course_no
    course = course_list[course_no]

    if (course_no == 1):
        return render(request, 'live_record.html', locals())
    else:
        return render(request, 'live.html', locals())
